io.stdout:setvbuf("no")

INF = 999999999

lib = "lib."
class = require (lib.."hump.class")
State = require (lib.."hump.gamestate")
Signal = require (lib.."hump.signal")
Timer = require (lib.."hump.timer")
Vector = require (lib.."vecffi")
VecL = require (lib.."hump.vector-light")
tactile = require (lib.."tactile")--Thread = require "toolchain.Thread"
utf8 = require "utf8"

Lume = require (lib.."lume")

StateMenu = require "state.menu"
StateGame = require "state.game"
StateGamepad = require "state.gamepadSettings"
StateDebug = require "state.debug"

math.randomseed(os.time())

function initGraphics()
    --love.graphics.setDefaultFilter("nearest", "nearest")
    WindowWidth, WindowHeight = love.graphics.getDimensions()
    GameWidth, GameHeight = 1280, 720

    spriteCursor = love.graphics.newImage("cursor.png")
    
    gameCanvas = love.graphics.newCanvas(GameWidth, GameHeight)
    love.graphics.setLineWidth(2)

    str = love.filesystem.read("CRT.frag")
    ShaderCRT = love.graphics.newShader(str)
	CRT_SIZE = 2
	ShaderCRT:send('inputSize', {GameWidth/CRT_SIZE, GameHeight/CRT_SIZE})
	ShaderCRT:send('textureSize', {GameWidth/CRT_SIZE, GameHeight/CRT_SIZE})


    blur = love.graphics.newShader(([[
		const float kernel[5] = float[](0.2270270270, 0.1945945946, 0.1216216216, 0.0540540541, 0.0162162162);
		vec4 effect(vec4 color, sampler2D tex, vec2 tex_coords, vec2 pos) {
			color = texture2D(tex, tex_coords) * kernel[0];
			for(int i = 1; i < 5; i++) {
				color += texture2D(tex, vec2(tex_coords.x + i * %f, tex_coords.y)) * kernel[i];
				color += texture2D(tex, vec2(tex_coords.x - i * %f, tex_coords.y)) * kernel[i];
			}
			return color;
		}
	]]):format(1/GameWidth,1/GameHeight))
end

function checkGamepads()
    GamepadsConfigured = true
    for i, pad in ipairs(Gamepads) do
        if not pad:isGamepad() then
            GamepadsConfigured = false
            State.switch(StateGamepad, pad)
        end
    end
end

function initControls()
    --GamepadMappings = require "gamepadMappings"
    GamepadsConfigured = false
    if love.filesystem.exists("mappings.txt") then
        love.joystick.loadGamepadMappings("mappings.txt")
    end
    Gamepads = love.joystick.getJoysticks()
    checkGamepads()
    --[[Control = {
        {
            MoveKeyboardH = tactile.newControl()
                :addButtonPair(tactile.keys('a', 'left'), tactile.keys('d', 'right')),
            MoveKeyboardV = tactile.newControl()
                :addButtonPair(tactile.keys('w', 'up'), tactile.keys('s', 'down')),
            MoveGamepadH = tactile.newControl()
                :addAxis(tactile.gamepadAxis(1, 'leftx')),
            MoveGamepadV = tactile.newControl()
                :addAxis(tactile.gamepadAxis(1, 'lefty')),
            LookHorizontal = tactile.newControl()
                :addAxis(tactile.gamepadAxis(1, 'rightx')),
            LookVertical = tactile.newControl()
                :addAxis(tactile.gamepadAxis(1, 'righty')),
            Fire = tactile.newControl()
                :addAxis(tactile.gamepadAxis(1, 'triggerright'))
        },
        {
            MoveGamepadH = tactile.newControl()
                :addAxis(tactile.gamepadAxis(2, 'leftx')),
            MoveGamepadV = tactile.newControl()
                :addAxis(tactile.gamepadAxis(2, 'lefty')),
            LookHorizontal = tactile.newControl()
                :addAxis(tactile.gamepadAxis(2, 'rightx')),
            LookVertical = tactile.newControl()
                :addAxis(tactile.gamepadAxis(2, 'righty')),
            Fire = tactile.newControl()
                :addAxis(tactile.gamepadAxis(2, 'triggerright'))
        }
    }]]
    GameKeys = {
        MoveH = tactile.newControl():addButtonPair(tactile.keys('a'), tactile.keys('d')),
        MoveV = tactile.newControl():addButtonPair(tactile.keys('w'), tactile.keys('s'))
    }

    mouseX, mouseY = WindowWidth / 2, WindowHeight / 2
    mouseV = Vector(mouseX, mouseY)
    mouseDx, mouseDy = 0, 0
    mouseD = Vector(0, 0)
    love.mouse.setRelativeMode(true)

    GamepadTriggers = {}
    for i, pad in ipairs(Gamepads) do
        GamepadTriggers[i] = {
            left = false,
            right = false
        }  
    end

    clearPressed()
end

function love.load()
    U = 32
    usedColors = {}
    State.registerEvents()
    initGraphics()
    initControls()
    if GamepadsConfigured then
        State.switch(StateGame)
    else
        --State.switch(StateGamepad)
    end
end

function love.resize(w, h)
    WindowWidth, WindowHeight = w, h
end

function love.update(dt) 
    DELTA = dt
    Timer.update(DELTA)
    --[[for i, v in ipairs(Control) do
        for j, k in pairs(v) do
            k:update()
        end
    end]]
    for i, pad in ipairs(Gamepads) do
        local trigleft = pad:getGamepadAxis("triggerleft")
        local trigright = pad:getGamepadAxis("triggerright")
        if trigleft > 0.5 then
            if not GamepadTriggers[i].left then
                GamepadTriggers[i].left = true
                gamepadPressed[i]["triggerleft"] = true
            end
        else
            GamepadTriggers[i].left = false
        end

        if trigright > 0.5 then
            if not GamepadTriggers[i].right then
                GamepadTriggers[i].right = true
                gamepadPressed[i]["triggerright"] = true
            end
        else
            GamepadTriggers[i].right = false
        end
    end
    for i, v in pairs(GameKeys) do
        v:update()
    end

end

function love.mousemoved(x, y, dx, dy, isTouch)
    if love.mouse.getRelativeMode() then
        mouseX, mouseY = Lume.clamp(mouseX + dx, 0, GameWidth), Lume.clamp(mouseY + dy, 0, GameHeight)
        mouseV.x, mouseV.y = mouseX, mouseY
        mouseDx, mouseDy = dx, dy
        mouseD.x, mouseD.y = dx, dy
    end
end

function love.keypressed(key)
    if key == "q" then
        love.mouse.setRelativeMode(not love.mouse.getRelativeMode())
    end
    if key == "`" then
        if State.current() ~= StateDebug then
            State.push(StateDebug)
        else
            State.pop()
        end
    end
    keyPressed[key] = true
end

function love.keyreleased(key)
    keyReleased[key] = true
end

function love.mousepressed(x, y, button, isTouch)
    mousePressed[button] = true
end

function love.mousereleased(x, y, button, isTouch)
    mouseReleased[button] = true
end

function love.gamepadpressed(pad, button)
    local id = pad:getID()
    print(button)
    gamepadPressed[id][button] = true
end

function love.gamepadreleased(pad, button)
    local id = pad:getID()
    gamepadReleased[id][button] = true
end

function love.draw()
    mouseDx, mouseDy = 0, 0
    beginDraw()
    setColor("ffffffff")
    love.graphics.draw(spriteCursor, round(mouseX, CRT_SIZE) , round(mouseY, CRT_SIZE), 0, 4, 4, 4, 4)
    endDraw()
    --love.graphics.setShader(ShaderCRT)
    local dw, dg = WindowWidth / WindowHeight, GameWidth / GameHeight
    local size = WindowWidth / GameWidth
    if dg < dw then
        size = WindowHeight / GameHeight
    end
    love.graphics.draw(gameCanvas, WindowWidth / 2, WindowHeight / 2, 0, size, size, GameWidth / 2, GameHeight / 2)
    love.graphics.setShader()

    clearPressed()
end

function clearPressed()
    keyPressed = {}
    keyReleased = {}
    mousePressed = {}
    mouseReleased = {}
    gamepadPressed = {}
    gamepadReleased = {}

    for i, pad in ipairs(Gamepads) do
        gamepadPressed[i] = {}
        gamepadReleased[i] = {}
    end
end

function beginDraw()
    love.graphics.push("all")
    love.graphics.setCanvas(gameCanvas)
end

function endDraw()
    love.graphics.setCanvas()
    love.graphics.pop()
end

function setColor(hex)
    if not usedColors[hex] then
        _,_,r,g,b,a = hex:find('(%x%x)(%x%x)(%x%x)(%x%x)')
        --print(r,g,b,a)
        if not a then a = "ff" end
        usedColors[hex] = {tonumber(r,16),tonumber(g,16),tonumber(b,16),tonumber(a,16)}
    end
    love.graphics.setColor(unpack(usedColors[hex]))
end

function round(x, n)
    return math.floor(x / n)*n
end

function randomizeName(str)
    return str .. math.random(0,999999999)
end

function axisWithDeadzone(gamepad, axis, deadzone)
    local a = gamepad:getGamepadAxis(axis)
    return (math.abs(a) > deadzone) and (a-Lume.sign(a)*deadzone) / (1 - deadzone) or 0
end

function axisSin(gamepad, axis)
    local a = gamepad:getGamepadAxis(axis)
    return math.sin(a*math.pi/2)
end