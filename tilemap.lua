local ffi = require "ffi"

local Tilemap = class{}

function Tilemap:init(width, height, tileWidth, tileHeight, fill)
	self.width = width
	self.height = height
	self.size = width * height - 1
	self.tileWidth = tileWidth or U
	self.tileHeight = tileHeight or	U
	self.tiles = ffi.new(string.format("int[%d]", width * height))
	for i = 0, self.size do
		self.tiles[i] = fill or 0
	end

	self.indexSize = 8
	self.props = {
		["solid"] = 1,
		["damage"] = 2,
		["slow"] = 4
	}
	for i, v in pairs(self.props) do 
		self.props[i] = bit.lshift(v, self.indexSize)
	end
end

function Tilemap:makeTile(id, ...)
	local i = id
	local a = {...}
	local props = {}
	for _, prop in ipairs(a) do
		--table.insert(props, self.props[v])
		i = i + self.props[prop]
	end
	--return id + unpack(props)
	return i
end

function Tilemap:getProp(id, prop)
	return bit.band(id, self.props[prop]) == self.props[prop]
end

function Tilemap:fill(func)
	for i = 0, self.size do
		self.tiles[i] = func(self, i)
	end
end

function Tilemap:x(i)
	return i % self.width
end

function Tilemap:y(i)
	return math.floor(i / self.height)
end

function Tilemap:id(x, y)
	return (x < 0 or x >= self.width or y < 0 or y >= self.height) and -1 or y * self.width + x
end

function Tilemap:data(i)
	return (i >= 0 and i <= self.size) and self.tiles[i] or 0
end

function Tilemap:get(x, y)
	return self:data(self:id(x, y))
end

function Tilemap:set(x, y, data)
	__i = self:id(x, y)
	if __i > -1 then
		self.tiles[__i] = data
	end
end

function Tilemap:idToCoords(i)
	return self:x(i), self:y(i)
end

function Tilemap:idToReal(i)
	__x, __y = self:idToCoords(i)
	return __x * self.tileWidth, __y * self.tileHeight
end

function Tilemap:realToTile(x, y)
	return math.floor(x / self.tileWidth), math.floor(y / self.tileHeight)
end

function Tilemap:realToId(x, y)
	return self:id(self:realToTile(x, y))
end

return Tilemap