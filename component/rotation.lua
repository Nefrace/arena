local Core = require "component.core"
local Rotation = class{__includes = Core}

Rotation.name = "Rotation"

function Rotation:init(parent, name, angle)
	Core.init(self, parent, name)
	self.active = true
	self.angle = angle or 0
end

function Rotation:update(dt)
	if self.active then
		self.parent.rotation = self.parent.rotation + self.angle * dt
	end
end

function Rotation:setActive(active)
	self.active = active
	return self
end

function Rotation:getActive()
	return self.active
end

function Rotation:set(angle)
	self.angle = angle or 0
	return self
end

function Rotation:get()
	return self.angle
end

function Rotation:add(angle)
	self.angle = self.angle + angle
	return self
end

return Rotation