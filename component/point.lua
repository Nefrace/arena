local Core = require "component.core"

local Point = class{__includes=Core}

function Point:init(parent, name, x, y, relative)
	Core.init(self, parent, name)
	self.position = Vector(x, y)
	self.rotation = 0--self.position:angle()
	self.__lastRot = 0
	self.relative = self.parent and relative or false
end

function Point:getPosition()
	local vec = self.position:clone()
	local rot = self.rotation
	local rel = self.relative
	if rel then
		local i = self.parent
		if i then
			while i do
				rot = rot + i.rotation
				i = i.parent
			end
		end
	end
	vec:rotate(rot)
	if rel then
		vec = vec + self.parent:getPosition()
	end
	return vec
end

function Point:setPosition(x, y)
	self.position.x, self.position.y = x, y
	--self.rotation = self.position:angle()
end

function Point:selfDraw()
	__drawVec = self:getPosition()
	--__parentPos = self.parent:getPosition()
	love.graphics.circle("fill", __drawVec.x, __drawVec.y, 2)
	--love.graphics.line(__drawVec.x, __drawVec.y, __parentPos.x, __parentPos.y)
	--love.graphics.print(self.rotation, __drawVec.x, __drawVec.y)
end

return Point