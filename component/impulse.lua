local Motion = require "component.motion"

local Impulse = class{}
Impulse:include(Motion)

function Impulse:init(parent, name, vector, slowdownTime)
	Motion.init(self, parent, name, vector, #vector)
	if #vector > 0 then
		self.initSpeed = #vector
		self.slowdownTime = slowdownTime
		self.slowdownTimeInit = slowdownTime
	else
		self:destroy()
	end
end

function Impulse:update(dt)
	Motion.update(self)
	if self.slowdownTime > 0 then
		self.slowdownTime = self.slowdownTime - dt
		self:setSpeed(self.initSpeed * (self.slowdownTime / self.slowdownTimeInit))
	else
		self:destroy()
	end
end

return Impulse