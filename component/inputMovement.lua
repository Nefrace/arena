local Core = require "component.core"

local InputMovement = class{__includes=Core}

function InputMovement:init(parent, name, motionComponent, playerID, gamepad)
	Core.init(self, parent, name)
	self.motionComponent = motionComponent or self.parent:getComponent("MainMotion")
	if not self.motionComponent then
		error("InputMovement: Parent expected", 2)
	end
	self.playerID = playerID or 1
	self.gamepad = gamepad
	self.mouseSensivity = 0.7
	self.gamepadSensivity = 10
	self.motionVector = Vector(0, 0)
	self.viewVector = Vector(1, 0)
	self.rotation = 0--self.position:angle()
	self.__lastRot = 0
	self.relative = self.parent and relative or false
end

function InputMovement:selfUpdate(dt)
	local ctrl = self.playerID
	if self.playerID == 2 and #Gamepads == 1 then
		ctrl = 1
	end
	local move = "Keyboard"
	if self.gamepad then
		move = "Gamepad"
	end
	self.motionVector.x = Control[ctrl]["Move"..move.."H"]()
	self.motionVector.y = Control[ctrl]["Move"..move.."V"]()

	if self.gamepad then
		--self.viewVector.x = Control[ctrl].LookHorizontal()
		--self.viewVector.y = Control[ctrl].LookVertical()
		self.parent.rotation = self.parent.rotation + Control[ctrl].LookHorizontal() * dt * self.gamepadSensivity
		if gamepadPressed[ctrl]["triggerright"] then
		end
	else
		--self.viewVector = mouseV - self.parent:getPosition()
		self.parent.rotation = self.parent.rotation + mouseDx * dt * self.mouseSensivity
	end
	self.motionVector:normalize()
	self.motionVector:rotate(self.parent.rotation + math.pi / 2)

	self.motionComponent.vector = self.motionVector * self.motionComponent.maxSpeed
	--self.parent.rotation = self.viewVector:angle()
end

return InputMovement