local Core = require "component.core"
local Rectangle = class{__includes = Core}

function Rectangle:init(parent, name, x, y, w, h)
	Core.init(self, parent, name)
	self.parent.collider = self
end