local Core = require "component.core"
local Motion = class{__includes = Core}

Motion.name = "Motion"

function Motion:init(parent, name, vec, maxSpeed)
	Core.init(self, parent, name)
	self.active = true
	self.vector = vec or Vector(0, 0)
	self.maxSpeed = maxSpeed or 999999
end

function Motion:set(a, b)
	if not b then
		self.vector.x, self.vector.y = a.x, a.y
	else
		self.vector.x, self.vector.y = a, b
	end
end

function Motion:add(a, b)
	if not b then
		self.vector.x, self.vector.y = self.vector.x + a.x, self.vector.y + a.y
	else
		self.vector.x, self.vector.y = self.vector.x + a, self.vector.y + b
	end
end

function Motion:get()
	return self.vector:clone()
end

function Motion:update(dt)
	if self.active then
		self.vector:trim(self.maxSpeed)
		local var = "position"
		if self.parent.motion then
			var = "motion"
		end
		self.parent[var] = self.parent[var] + self.vector
	end
	Core.update(self)
end

function Motion:setActive(active)
	self.active = active
	return self
end

function Motion:getActive()
	return self.active
end

function Motion:rotate(angle)
	self.vector:rotate(angle)
	return self
end

function Motion:getAngle()
	return self.vector:angle()
end

function Motion:setAngle(angle)
	self.vector:setAngle(angle)
	return self
end

function Motion:targetTo(target, speed)
	--[[__vecdiff = target - self.parent.position
	self:rotateTo(__vecdiff:angle())
	if speed then
		self:setSpeed(speed)
	end]]
	__veclen = #self.vector
	self.vector.x = target.x - self.parent.position.x
	self.vector.y = target.y - self.parent.position.y
	self:setSpeed(speed or __veclen)
	return self
end

function Motion:setSpeed(speed)
	self.vector:normalize(speed)
	return self
end

function Motion:getSpeed()
	return #self.vector
end

return Motion