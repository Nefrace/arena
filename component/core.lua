local Component = class{component = true}

function Component:init(parent, name)
	self.componentName = name or self.name
	if parent then
		self.parent = parent
		self.parent.component[self.componentName] = self
	end
	self.component = {}
	Signal.emit(self.componentName.."Init", self)
end

function Component:addComponent(component, ...)
	if component and component.component then
		return component(self, ...)
	end
	error("Component:addComponent: component expected, got "..type(component), 2)
end

function Component:getComponent(name)
	return self.component[name]
end

function Component:removeComponent(name)
	if self.component[component.name] then
		self.component[component.name]:destroy()
	end
	return self
end

function Component:update(dt)
	--Signal.emit(self.componentName.."Update", self)
	self:selfUpdate(dt)
	self:updateComponents(dt)
end

function Component:updateComponents(dt)
	for i, v in pairs(self.component) do
		if v.update then
			v:update(dt)
		end
	end
end

function Component:destroy()
	if self.parent then
		self.parent.component[self.componentName] = nil
		self.parent = nil
	end
	for i, v in pairs(self.component) do
		self.component[i]:destroy()
		self.component[i] = nil
	end
end

function Component:draw()
	self:selfDraw()
	self:drawComponents()
end

function Component:selfUpdate(dt) end

function Component:selfDraw() end

function Component:drawComponents()
	for i, v in pairs(self.component) do
    	if v.draw then
    		v:draw()
    	end
    	--Signal.emit(self.componentName.."Draw", self)
    end
end

return Component