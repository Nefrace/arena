local Game = {}

local Actor = require "entity.actor"
local Motion = require "component.motion"
local Point = require "component.point"
local Rotation = require "component.rotation"
local Input = require "component.inputMovement"
local Gamera = require "lib.gamera"
local Bump = require "lib.bump"

local Tilemap = require "tilemap"

local Player = require "entity.player"
local Enemy = require "entity.runner"

function Game:enter(from, settings)
	camera = {}

	World = Bump.newWorld(50)
	
	act = {}
	entities = {}
	entities.players = {}
	entities.disks = {}
	entities.enemies = {}
	entities.pickups = {}
	self.players = 0
	--[[local cameraWidth = self.players == 2 and GameWidth / 2 - 2 or GameWidth
	for i = 1, self.players do
		camera[i] = Gamera.new(-1000, -1000, 9999, 9999)
		camera[i]:setWindow(({0, GameWidth/2+2})[i], 0, cameraWidth, GameHeight)
		camera[i]:setScale(1.5)
		act[i] = Player(i, settings.players[i].gamepad, Vector.fromPolar(i*30 / 180 * math.pi, 100))

		--table.insert(entities, act[i])
		--table.insert(entitiesDynamic, act[i])
	end]]

	ControlsBinded = {keyboard = false, gamepads = {}}
	for i, v in ipairs(Gamepads) do
		ControlsBinded.gamepads[i] = false
	end

	SafeZone = {Name = "Home", type = "home", x = 500, y = 500, w = 300, h = 300, position = Vector(500, 500)}
	World:add(SafeZone, SafeZone.x, SafeZone.y, SafeZone.w, SafeZone.h)

	rects = {}
	for i = 1, 20 do
		local rect = {Name = "block", type = "wall", x = math.random(0, 3000), y = math.random(0, 3000), w = math.random(20,150), h = math.random(20, 150)}
		World:add(rect, rect.x, rect.y, rect.w, rect.h)
		World:move(rect, rect.x, rect.y)
		table.insert(rects, rect)
	end


	for i = 1, 10 do
		Enemy(Vector(math.random(0, 1000), math.random(0,1000)))
	end

	tilemap = Tilemap(20, 20)
	tilemap:fill(function(t, i)
		local x, y = t:idToCoords(i)
		if x == 0 or y == 0 or x == t.width - 1 or y == t.height - 1 then-- or math.random(16) == 1 then
			--return 257--tilemap:makeTile(1, "solid")
		end
		return 0
	end)

	time = 0
	GameStarted = false
end

function Game:keypressed(key)
	if key == "g" then
		State.switch(StateGamepad, Gamepads[1])
	end
	if key == "escape" then
		State.switch(StateMenu)
	end
end

function Game:update(dt)
	time = time + dt

	if not GameStarted then
		for i, pad in ipairs(Gamepads) do
			if gamepadPressed[i]["a"] and not ControlsBinded.gamepads[i] then
				local plr = Player(#entities.players+1, i, SafeZone.position + Vector(150, 150)+Vector.fromPolar(#entities.players*30/180*math.pi, 100))
				ControlsBinded.gamepads[i] = plr
				local cam = Gamera.new(-1000, -1000, 9999, 9999)
				cam:setScale(1.5)
				cam:setWindow(0, 0, GameWidth, GameHeight)
				if camera[1] then 
					camera[1]:setWindow(0, 0, GameWidth / 2, GameHeight)
					cam:setWindow(GameWidth / 2, 0, GameWidth / 2, GameHeight)
				end	
				table.insert(camera, cam)
			end
		end
		if keyPressed["e"] and not ControlsBinded.keyboard then
			local plr = Player(#entities.players+1, nil, SafeZone.position + Vector(150, 150)+Vector.fromPolar(#entities.players*30/180*math.pi, 100))
			ControlsBinded.keyboard = plr
			local cam = Gamera.new(-1000, -1000, 9999, 9999)
			cam:setScale(1.5)
			cam:setWindow(0, 0, GameWidth, GameHeight)
			if camera[1] then 
				camera[1]:setWindow(0, 0, GameWidth / 2, GameHeight)
				cam:setWindow(GameWidth / 2, 0, GameWidth / 2, GameHeight)
			end	
			table.insert(camera, cam)
		end
 	end

	entities.remove = {}
	for i = 1, #entities do
		local act = entities[i]
		act:update(dt)
	end
	for i, objectToRemove in ipairs(entities.remove) do
		for j, objectInTable in ipairs(objectToRemove.table) do
			if objectToRemove.object == objectInTable then
				table.remove(objectToRemove.table, j)
				break
			end
		end
	end
end

function drawWorld(l, t, w, h)
	local cx, cy = l+w/2, t+h/2
	local Center = Vector(cx, cy)

	setColor("00ffff22")
	love.graphics.setLineWidth(1)

	--Drawing grid
	for x = l, l+w, 50 do
		love.graphics.line(x-(l%50), t, x-(l%50), t+h)
	end
	for y = t, t+h, 50 do
		love.graphics.line(l, y-(t%50), l+w, y-(t%50))
	end

	setColor("ffffffff")
	love.graphics.setLineWidth(2)
	local entities, len = World:queryRect(l, t, w, h)
	for i, act in ipairs(entities) do
		love.graphics.push("all")
		if act.draw then
			act:draw()
		end
		love.graphics.pop()
		if act.type == "wall" then
			local x,y,w,h = World:getRect(act)
			local xc, yc = x + w / 2, y + h / 2
			local x2 = x + (x - cx) / 5
			local y2 = y + (y - cy) / 5
			local xw2 = x + w + (x + w - cx) / 5
			local yh2 = y + h + (y + h - cy) / 5
			local w2, h2 = xw2 - x2, yh2 - y2
			
			love.graphics.rectangle("line", x, y, w, h)
			love.graphics.rectangle("line", x2, y2, w2, h2)
			love.graphics.line(x, y, x2, y2)
			love.graphics.line(x+w, y, xw2, y2)
			love.graphics.line(x, y+h, x2, yh2)
			love.graphics.line(x+w, y+h, xw2, yh2)
			love.graphics.circle("fill", cx, cy, 3)
		end
	end
	--[[for i = 0, tilemap.size do
    	local x, y = tilemap:idToReal(i)
    	local d = tilemap:data(i)
    	if d > 0 then
    		setColor("aaaaffff")
    		love.graphics.rectangle("fill", x, y, U, U)
    		setColor("ffffffff")
    		love.graphics.print(tilemap:data(i), x, y)
    	end
    end]]
end

function Game:draw()
    beginDraw()
        setColor("201070ff")
        love.graphics.rectangle("fill", 0, 0, GameWidth, GameHeight)
        setColor("ffffffff")
        for i = 1, #entities.players do
        	local player = entities.players[i]
        	local p = player:getComponent("cameraPoint")
        	local lookVector = p:getPosition()
        	lookVector = lookVector - player.position
			camera[i]:setPosition(player.position.x+lookVector.x, player.position.y+lookVector.y)
			if player.rotateCamera then
        		camera[i]:setAngle(player.rotation + math.pi/2)
        	else
        		camera[i]:setAngle(0)
        	end
        	camera[i]:draw(drawWorld)
        end
    endDraw()
end

return Game
