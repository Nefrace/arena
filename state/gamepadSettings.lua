local Gamepad = {}

function Gamepad:enter(from, gamepad)
	self.gamepad = gamepad
	self.axis = {}
	self.axisCount = gamepad:getAxisCount()
	for i = 1, self.axisCount do
		self.axis[i] = 0
	end
	self.buttons = {}
	self.buttonsPrevious = {}
	self.buttonsPressed = {}
	self.buttonsCount = gamepad:getButtonCount()
	for i = 1, self.buttonsCount do
		self.buttons[i] = false
		self.buttonsPrevious[i] = false
		self.buttonsPressed[i] = false
	end
	self.barWidth = 30
	self.barHeight = 200
	self.from = from

	self.virtualPad = {'leftx', 'lefty', 'rightx', 'righty', 'triggerleft', 'triggerright', 'a', 'b', 'x', 'y', 'start', 'back'}
	self.virtualPadAxis = {true, true, true, true, false, false, false, false, false, false, false, false}
	self.virtualPadNames = {'Left X-axis', 'Left Y-axis', 'Right X-axis', 'Right Y-axis', 'Left trigger', 'Right trigger', '"A" button', '"B" button', '"X" button', '"Y" button', '"Start" button', '"Back button'}
	self.mapping = {}
	self.currentButton = 1
	self.time = 10
	self.font = love.graphics.newFont(20)
end

function Gamepad:update(dt)

	for i = 1, self.axisCount do
		self.axis[i] = self.gamepad:getAxis(i)
	end
	for i = 1, self.buttonsCount do
		self.buttonsPressed[i] = false
		self.buttonsPrevious[i] = self.buttons[i]
		self.buttons[i] = self.gamepad:isDown(i)
		if self.buttons[i] and not self.buttonsPrevious[i] then
			self.buttonsPressed[i] = true
		end
	end
	self:configure()
end

function Gamepad:configure()
	local buttonPressed = 0
	for i = 1, self.buttonsCount do
		if self.buttonsPressed[i] == true then
			buttonPressed = i
			break
		end
	end
	if buttonPressed > 0 and self.currentButton <= #self.virtualPad then
		if not self.virtualPadAxis[self.currentButton] then
			self.mapping[self.virtualPad[self.currentButton]] = {"button", buttonPressed}
		else
			local maxAxis = 0
			local maxAxisValue = 0
			for i = 1, self.axisCount do
				if math.abs(self.axis[i]) > maxAxisValue then
					maxAxis = i
					maxAxisValue = math.abs(self.axis[i])
				end
			end
			if self.maxAxis ~= 0 then
				self.mapping[self.virtualPad[self.currentButton]] = {"axis", maxAxis}
			end
		end
		self.currentButton = self.currentButton + 1
		print(#self.mapping)
	end
	if self.currentButton > #self.virtualPad then
		for virtual, map in pairs(self.mapping) do
			if map[2] ~= 0 then
				love.joystick.setGamepadMapping(self.gamepad:getGUID(), virtual, map[1], map[2])
			end
		end
		local str = love.joystick.saveGamepadMappings("mappings.txt")
		f = io.open("mapping.txt", "w")
		f:write(str)
		f:flush()
		f:close()
		checkGamepads()
		State.switch(StateGame)
	end
end

function Gamepad:draw()
	beginDraw()
	setColor('335533ff')
	love.graphics.rectangle('fill', 0, 0, GameWidth, GameHeight)
	for i = 1, self.axisCount do
		love.graphics.push('all')
		setColor('000000ff')
		love.graphics.rectangle('fill', i * (self.barWidth+20), 100, self.barWidth, self.barHeight)
		setColor('00ff00ff')
		love.graphics.rectangle('fill', i * (self.barWidth+20), 100+self.barHeight/2, self.barWidth, -self.barHeight*self.axis[i]/2)
		love.graphics.pop()
	end

	for i = 1, self.buttonsCount do
		local x = 400 + ((i-1) % 5) * 40
		local y = 100 + (math.floor((i-1) / 5)*40)
		love.graphics.push('all')
		setColor('222222ff')
		if self.buttons[i] then
			setColor('22ff22ff')
		end
		love.graphics.circle('fill', x, y, 14)
		love.graphics.pop()
	end
	setColor("ffffffff")
	love.graphics.setFont(self.font)
	if self.currentButton <= #self.virtualPad then
		love.graphics.print("Select "..(self.virtualPadAxis[self.currentButton] and "axis " or "button ").."for "..self.virtualPadNames[self.currentButton], 100, 400)
	end
	
	endDraw()
	setColor("ffffffff")

	local y = 0
	for i, v in pairs(self.mapping) do
		local str = tostring(i)..": "
		if type(v) ~= "table" then
			str = str .. tostring(v) .. "\n"
		else
			str = str .. v[1] .. ": ".. v[2] .. "\n"
		end
		love.graphics.print(str, 0, y)
		y = y + 15
	end
end

return Gamepad