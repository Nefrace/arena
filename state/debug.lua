local Debug = {}

function Debug:enter(from)
	self.from = from
	self.line = ""
	if not self.commandHistory then self.commandHistory = {} end
	if not self.lines then self.lines = {} end
	self.lineCursor = 1
end

function Debug:textinput(t)
    self.line = self.line .. t
end
 
function Debug:keypressed(key)
    if key == "backspace" then
        -- get the byte offset to the last UTF-8 character in the string.
        local byteoffset = utf8.offset(self.line, -1)
 
        if byteoffset then
            -- remove the last UTF-8 character.
            -- string.sub operates on bytes rather than UTF-8 characters, so we couldn't do string.sub(text, 1, -2).
            self.line = string.sub(self.line, 1, byteoffset - 1)
        end
    end

    if key == "return" then
    	local command = loadstring(self.line)
    	status, err = pcall(command)
    	table.insert(self.commandHistory, self.line)
    	local str = "["..string.upper(tostring(status)).."]"
    	if err then
    		str = str .. " "..err
    	end
    	table.insert(self.lines, string.format(">%s: %s", self.line, str))
    	self.line = ""
    	self.lineCursor = 1
    	--State.pop()
    end
end

function Debug:draw()
	self.from:draw()
	love.graphics.push("all")
	setColor("00000055")
	love.graphics.rectangle("fill", 0, 0, GameWidth, 300)
	setColor("999999ff")
	love.graphics.rectangle("line", 0, 0, GameWidth, 300)
	setColor("ffffffff")
	love.graphics.print(self.line, 0, 0)
	for i, v in ipairs(self.lines) do
		love.graphics.print(v, 0, 10+i*12)
	end
	love.graphics.pop()
end

return Debug