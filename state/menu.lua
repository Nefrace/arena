local Menu = {}

function Menu:enter()
	self.menu = {}
	self.menu[1] = self:newButton("start", "Start game")
	self.menu[2] = self:newButton("players", "Players", {min=1, max=2, step=1, current=1})
	self.menu[3] = self:newButton("player1gp", "1'st player controls", {false, true, current=1}, {"Keyboard/mouse", "Gamepad"})
	self.menu[4] = self:newButton("player2gp", "2'st player controls", {false, true, current=2}, {"Keyboard/mouse", "Gamepad"})
	self.selected = 1
	self.players = 1
	self.font = love.graphics.newFont(26)
end

function Menu:newButton(name, text, values, printedValues)
	local button = {
		name = name,
		text = text,
		values = values,
		printedValues = printedValues or values
	}
	return button
end

function Menu:getButton(name, buttons)
	for i, button in ipairs(buttons or self.menu) do
		if button.name == name then
			return button
		end
	end
	return nil
end

function Menu:getValue(name, buttons)
	local button = self:getButton(name, buttons)
	--print(button)
	if button then
		if button.values.min then
			return button.values.current
		end
		return button.values[button.values.current]
	end
	return nil
end

function Menu:update(dt)

end

function Menu:keypressed(key)
	if key == "down" then
		self.selected = self.selected + 1
		if self.selected > #self.menu then
			self.selected = 1
		end
	end

	if key == "up" then
		self.selected = self.selected - 1
		if self.selected < 1 then
			self.selected = #self.menu
		end
	end

	local selected = self.menu[self.selected]
	if selected.values then
		local values = selected.values
		local shift = 0
		if key == "left" then
			shift = -1
		elseif key == "right" then
			shift = 1
		end
		if values.min and values.max then
			local step = values.step or 1
			values.current = Lume.clamp(values.current + shift * step, values.min, values.max)
		else
			values.current = values.current + shift
			if values.current < 1 then values.current = #values end
			if values.current > #values then values.current = 1 end
		end
	end
	if key == "return" then
		if selected.name == "start" then
		local Settings = {}
		Settings.players = {}
		for i = 1, self:getValue("players") do
			Settings.players[i] = {}
			Settings.players[i].gamepad = self:getValue("player"..i.."gp")
		end
		State.switch(StateGame, Settings)
		end
	end
end

function Menu:draw()
    beginDraw()
        setColor("cc2233ff")
        love.graphics.setFont(self.font)
        love.graphics.rectangle("fill", 0, 0, GameWidth, GameHeight)
        for i, menu in ipairs(self.menu) do
        	setColor("ffffffff")
        	local text = menu.text
        	if menu.values then
        		text = text..": "
        		if menu.values.min then
        			text = text..menu.values.current
        		else
        			text = text..menu.printedValues[menu.values.current]
        		end
        	end
        	if self.selected == i then
        		love.graphics.rectangle("fill", 100, 100 + i * 40, self.font:getWidth(text), self.font:getHeight(text))
        		setColor("cc2233ff")
        	end
        	love.graphics.print(text, 100, 100 + i * 40)
        end
    endDraw()
end

return Menu