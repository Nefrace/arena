local Actor = require "entity.actor"
local Point = require "component.point"

local Enemy = class{}
Enemy:include(Actor)

function Enemy:init(position)
	Actor.init(self, position, 14)
	self.type = "enemy"
	self.seePlayer = false
	self.nearestPlayer = nil
	self.angleToPlayer = 0
	self.vectorToPlayer = Vector(0, 0)
	self:addComponent(Point, "LookAtPoint", 30, 0, true)
	table.insert(entities.enemies, self)
end

function Enemy:update(dt)
	Actor.update(self, dt)
end

function Enemy:getNearestPlayer()
	local distance = INF
	local nearestPlayer = nil
	for i, player in ipairs(entities.players) do
		local vec = player.position - self.position
--		print(string.format("player: %s\tposition: %s\tself: %s\tdistance: %d\tvector: %s", player, player.position, self.position, distance, vec))
		if vec:len2() < distance then
			nearestPlayer = player
			distance = vec:len2()
		end
	end
	return nearestPlayer
end

function Enemy:destroy()
	Actor.destroy(self)
	table.insert(entities.remove, {table = entities.enemies, object = self})
end

function Enemy:draw()
	setColor("ff8888ff")
	love.graphics.push()
	love.graphics.translate(self.position.x, self.position.y)
	love.graphics.rotate(self.rotation)
	love.graphics.circle("fill", 0, 0, self.radius, 6)
	love.graphics.line(0,0,30,0)
	love.graphics.pop()
end

return Enemy