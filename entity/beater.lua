local Enemy = require "entity.enemy"
local Motion = require "component.motion"
local Impulse = require "component.impulse"

local Beater = class{}
Beater:include(Enemy)

function Beater:init(position)
	Enemy.init(self, position)
	self.move = self:addComponent(Motion, "motion", Vector(200,0), 200)
	self.filter = function(item, other)
		if other.name == "Laser" then return nil end
		if other.type == "wall" then return "slide" end
		return "cross"
	end

	self.sinAngle = 0
end

function Beater:update(dt)
	Enemy.update(self,dt)
	self.sinAngle = math.sin(time*math.pi)/3
	
	self.nearestPlayer = Enemy.getNearestPlayer(self)
	if self.nearestPlayer then
		--self.move:targetTo(self.nearestPlayer.position, 200)
	end
	--self.move:rotateTo(self.move:getAngle() + self.sinAngle)
	self.move:add((self.nearestPlayer.position - self.position):normalized(1500*dt))
	self.rotation = (self.nearestPlayer.position - self.position):angle()
end

function Beater:collisionCallback(collision)
	if collision.type == "slide" then
		local slide = Vector(collision.slide.x, collision.slide.y)
		local touch = Vector(collision.touch.x, collision.touch.y)
		local diff = slide - touch
		self.move:set(diff.x / DELTA, diff.y / DELTA)
	else
		local other = collision.other
		local diff = other.position - self.position
		local rads = other.radius + self.radius
		local radsVector = diff:normalized(rads)
		if diff:len() < rads then
			self:addComponent(Impulse, randomizeName("pulse"), (-radsVector+diff)*3, 0.5)
		end
		if other.type == "player" then
			other:hit(self)
		end
	end
end

return Beater