local Actor = require "entity.actor"

local Disk = class{}
Disk:include(Actor)

local Motion = require "component.motion"
local Point = require "component.point"

function Disk:init(player, position, direction, speed)
	Actor.init(self, position, 6)
	self.type = "disk"
	self.player = player
	self:addComponent(Motion, "motion", Vector.fromPolar(direction, speed), 1000)
	self.isMoving = true
	self.returning = false
	self.bounces = 3
	self.filter = function(item, other)
		if other.type == "wall" then return self.bounces > 0 and "bounce" or "touch" end
		return "cross"
	end
	Timer.after(0.4, function() self.returning = true end)
	table.insert(entities.disks, self)
end

function Disk:tilemapCallback(tile, collisionVector)
	if tilemap:getProp(tile, "solid") then
		self:removeComponent("motion")
		--self:destroy()
		self.isMoving = false
	end
end

function Disk:collisionCallback(collision)
	if collision.type == "touch" then
		self:getComponent("motion"):setActive(false)
		self.isMoving = false
	elseif collision.type == "bounce" then
		local bounceVector = Vector(collision.bounce.x, collision.bounce.y)
		local touchVector = Vector(collision.touch.x, collision.touch.y)
		self.bounces = self.bounces - 1
		self:getComponent("motion"):setAngle((bounceVector - touchVector):angle())
	elseif collision.type == "cross" then
		if collision.other.type == "enemy" then
			collision.other:destroy()
		end
	end
end

function Disk:update(dt)
	Actor.update(self, dt)
	if self.isMoving then
		local m = self:getComponent("motion")
		if self.returning then
			if m then
				local mv = m:get()
				m:set(mv:alerp(self.player.position - self.position, 5*dt, dt))
			end
		end
	end
end

function Disk:destroy()
	Actor.destroy(self)
	table.insert(entities.remove, {table = entities.disks, object = self})
end

function Disk:draw()
	love.graphics.circle("line", self.position.x, self.position.y, self.radius+4, 6)
end

return Disk