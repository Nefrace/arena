local Enemy = require "entity.enemy"
local Motion = require "component.motion"
local Impulse = require "component.impulse"

local Runner = class{}
Runner:include(Enemy)

function Runner:init(position)
    Enemy.init(self, position)
    self.move = self:addComponent(Motion, "motion", Vector(100, 0), 800)
    self.run = false
    self.filter = function(item, other)
        if other.name == "playerRaycast" then return nil end
        if other.type == "wall" or other.type == "home" then return "slide" end
        return "cross"
    end

    self.seePlayer = false
    self.playerRaycast = World:add({name = "playerRaycast", start = self.position, ending = Vector(0, 0)}, self.position.x, self.position.y, 1, 1)
    self.raycastFilter = function(item, other)
        return other.type == "wall" and "touch" or "cross"
    end
    self.attackTimer = 0
    self.timeToAttack = 1
end

function Runner:update(dt)
    Enemy.update(self, dt)
    self.nearestPlayer = Enemy.getNearestPlayer(self)
    if self.nearestPlayer then
        self.playerRaycast.ending = self.nearestPlayer.position:clone()
        self.playerRaycast.start = self.position:clone()
        World:update(self.playerRaycast, self.position.x, self.position.y)
        self.seePlayer = false
        local actX, actY, cols, len = World:check(self.playerRaycast, self.nearestPlayer.position.x, self.nearestPlayer.position.y, self.raycastFilter)
        for i = 1, len do
            local c = cols[i]
            if c.other.type == "player" then
                self.seePlayer = true
            end
        end

        if self.seePlayer then
            self.attackTimer = self.attackTimer + dt
            if self.attackTimer > self.timeToAttack and not self.attack then
                self.attack = true
                self.attackVector = self.nearestPlayer.position - self.position
                self.attackTimer = 0
            end
        else
            self.attackTimer = 0
            self.attack = false
        end
        local delta = self.nearestPlayer.position - self.position
        if not self.attack then
            self.move:set(delta:normalized(100))
        else
            self.move:set(self.attackVector:normalized(350))
            self.attackTimer = self.attackTimer + dt
            if self.attackTimer > 3 then
                self.attack = false
                self.attackTimer = 0
            end
        end

        self.rotation = delta:angle()
    end
end

function Runner:collisionCallback(collision)
    if collision.type == "slide" then
        local slide = Vector(collision.slide.x, collision.slide.y)
		local touch = Vector(collision.touch.x, collision.touch.y)
		local diff = slide - touch
		self.move:set(diff.x / DELTA, diff.y / DELTA)
    else
        local other = collision.other
		local diff = other.position - self.position
		local rads = other.radius + self.radius
		local radsVector = diff:normalized(rads)
		if diff:len() < rads then
			self:addComponent(Impulse, randomizeName("pulse"), (-radsVector+diff)*3, 0.5)
		end
		if other.type == "player" and self.run then
			other:hit(self)
            self.run = false
		end
    end
end

function Runner:draw()
    setColor(self.seePlayer and "ff4040ff" or "804040ff" )
    love.graphics.circle("fill", self.position.x, self.position.y, self.radius, 5)
    setColor("000000ff")
    love.graphics.print(self.attack and "0" or ".", self.position.x, self.position.y)
end

return Runner