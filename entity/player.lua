local Actor = require "entity.actor"

local Disk = require "entity.disk"

local Motion = require "component.motion"
local Point = require "component.point"
local Input = require "component.inputMovement"
local Impulse = require "component.impulse"

local Player = class{}
Player:include(Actor)

function Player:init(number, gamepad, position)
	Actor.init(self, position, 14)
	self.type = "player"
	self.number = number
	self.gamepad = gamepad
	self:addComponent(Motion, "keyMotion", Vector(0,0), 350)
	self:addComponent(Point, "cameraPoint", 100, 0, true)
	self:addComponent(Point, "diskPoint", 14, 0, true)
	self:addComponent(Point, "laserPoint", 1000, 0, true)
	self.diskCount = 2
	self.diskSpeed = 0
	self.diskSpeedMax = 600
	self.throwing = false
	self.filter = function(item, other)
		if other.type == "wall" then return "slide" end
		return "cross"
	end
	table.insert(entities.players, self)
	self.hited = false
	self.triggerPressed = 0
	self.rotateCamera = true
end

function Player:update(dt)
	--local ctrl = self.number
	local motionVector = Vector(0, 0)

	if self.throwing then
		if self.diskSpeed < self.diskSpeedMax then
			self.diskSpeed = self.diskSpeed + self.diskSpeedMax * dt
		else
			self:throwDisk()
		end
	end

	if self.gamepad then
		local gp = Gamepads[self.gamepad]
		local motionX = axisWithDeadzone(gp, "leftx", 0.2)
		local motionY = axisWithDeadzone(gp, "lefty", 0.2)
		local throwingTrigger = axisWithDeadzone(gp, "triggerright", 0.5)
		motionVector.x = motionX
		motionVector.y = motionY

		if self.rotateCamera then
			local rotationSpeed = axisWithDeadzone(gp, "rightx", 0.3)
			rotationSpeed = rotationSpeed * 10 * dt
			self.rotation = self.rotation + rotationSpeed
			local point = self:getComponent("cameraPoint")
			point.relative = true
			point:setPosition(100, 0)
		else
			local angleX = axisWithDeadzone(gp, "rightx", 0.1)
			local angleY = axisWithDeadzone(gp, "righty", 0.1)
			self.rotation = Vector(angleX, angleY):angle()
			local point = self:getComponent("cameraPoint")
			point.relative = false
			point:setPosition(self.position.x + angleX * 200, self.position.y + angleY * 200)
		end
		if throwingTrigger > 0 then
			if self.diskCount > 0 and self.triggerPressed == 0 then
				self.throwing = true
			end
			self.triggerPressed = 1
		else 
			self:throwDisk()
			self.triggerPressed = 0
		end
		if gamepadPressed[self.gamepad]["rightstick"] then
			self.rotateCamera = not self.rotateCamera
		end
	else
		motionVector.x = GameKeys.MoveH()
		motionVector.y = GameKeys.MoveV()
		if self.rotateCamera then
			self.rotation = self.rotation + mouseDx * dt * 0.6 --self.mouseSensivity
		end
		if mousePressed[1] then
			self.throwing = true
		elseif mouseReleased[1] then
			self:throwDisk()
		end
	end
	--motionVector:normalize()
	if self.rotateCamera then
		motionVector:rotate(self.rotation + math.pi / 2)
	end

	local motion = self:getComponent("keyMotion")
	motion.vector = motionVector * motion.maxSpeed
	--self.parent.rotation = self.viewVector:angle()


	Actor.update(self, dt)
	if keyPressed["e"] then
		--self:addComponent(Impulse, "pulse"..math.random(1,999999), Vector(100, 100), 0.5)
	end
	for i, disk in ipairs(entities.disks) do
		local vectorDiff = disk.position - self.position
		if vectorDiff:len2() < (self.radius + disk.radius)^2 then
			if disk.player == self and (not disk.isMoving or disk.returning)then
				disk:destroy()
				self.diskCount = self.diskCount + 1
			end
		end
	end
end

function Player:hit(other)
	if not self.hited then
		local delta = self.position - other.position
		delta:normalize(800)
		self:addComponent(Impulse, "HitImpulse", delta, 0.3)
		self.hited = true
		Timer.after(1, function() self.hited = false end)
	end
end

function Player:throwDisk()
	self.diskSpeed = math.max(200, self.diskSpeed)
	if self.diskCount > 0 and self.throwing and self.diskSpeed >= 200 then
		Disk(self, self:getComponent("diskPoint"):getPosition(), self.rotation, self.diskSpeed)
		self.diskCount = self.diskCount - 1
		self.diskSpeed = 0
	end
	self.throwing = false
end

function Player:destroy()
	Actor.destroy(self)
	table.insert(entities.remove, {table = entities.players, object = self})
end

function Player:draw()
	love.graphics.translate(self.position.x, self.position.y)
	--love.graphics.rotate(camera[self.number]:getAngle())
	setColor("00ffffff")
	love.graphics.circle("fill", 0, 0, self.radius)
	setColor("000000ff")
	love.graphics.print(self.number, 0, 0)
end

return Player