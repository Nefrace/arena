local Actor = class{}

function Actor:init(vector, radius)
	self.actor = true
    self.position = vector or Vector(0, 0)
    self.positionPrevious = Vector(x or 0, y or 0)
    self.motion = Vector(0, 0)
    self.solid = true
    self.rotation = 0
    self.depth = 0
    self.radius = radius or 16
    self.diameter = radius * 2
    self.component = {}
    World:add(self, self.position.x - self.radius, self.position.y - self.radius, self.diameter, self.diameter)
    self.filter = function(item, other) return "slide" end
    self.destroying = false

	table.insert(entities, self)
	self.id = #entities
end

function Actor:getPosition()
	return self.position:clone()
end

function Actor:addComponent(component, ...)
	if component and component.component then
		return component(self, ...)
	end
	error("Actor:addComponent: component expected, got "..type(component), 2)
	--return self
end

function Actor:getComponent(name)
	return self.component[name]
end

function Actor:removeComponent(name)

		if self.component[name] then
			self.component[name]:destroy()
			self.component[name] = nil
		end

	return self
end

function Actor:destroy()
	if not self.destroying then
		World:remove(self)
		for i, v in pairs(self.component) do
			self:removeComponent(i)
		end
		table.insert(entities.remove, {table=entities, object=self})
		self.destroying = true
	end
end

function Actor:update(dt)
	if self.destroying then return end
	self.motion.x, self.motion.y = 0, 0
	self:updateComponents(dt)
	self.positionPrevious.x, self.positionPrevious.y = self.position.x, self.position.y
	self.newPosition = self.position + self.motion:trimmed(1000) * dt

	local newX, newY, cols, len = World:move(self, self.newPosition.x - self.radius, self.newPosition.y - self.radius, self.filter)
	if self.collisionCallback then
		for i = 1, len do
			self:collisionCallback(cols[i])
		end
	end
	self.newPosition.x, self.newPosition.y = newX + self.radius, newY + self.radius
	local p = self.newPosition
	local r = self.radius
	local r2 = r * r
	drawMode = "line"
	local tiles = {}
	for dx = -1, 1 do
		for dy = -1, 1 do
			tileX, tileY = tilemap:realToTile(p.x, p.y)
			local t = {x = tileX + dx, y = tileY + dy}
			t.data = tilemap:get(t.x, t.y)
			local tileVector = Vector(t.x * tilemap.tileWidth + tilemap.tileWidth / 2, t.y * tilemap.tileHeight + tilemap.tileHeight / 2)
			t.distance = (tileVector - p):len2()
			table.insert(tiles, t)
		end
	end
	table.sort(tiles, function(a,b) return a.distance < b.distance end)
	for i, t in pairs(tiles) do
		--local tile = tilemap:get(t.x, t.y)
		--if tilemap:getProp(t.data, "solid") then
			local realX, realY = (t.x) * tilemap.tileWidth, (t.y) * tilemap.tileHeight
			local tileW, tileH = tilemap.tileWidth, tilemap.tileHeight
			local p = self.newPosition
			local bx = Lume.clamp(p.x, realX, realX + tileW)
			local by = Lume.clamp(p.y, realY, realY + tileH)
			local collisionVector = Vector(bx, by) - self.newPosition
			if collisionVector:len2() < r2 then
				local props = t.data
				if tilemap:getProp(t.data, "solid") then
					drawMode = "fill"
					if self.tilemapCallback then
						self:tilemapCallback(t.data, collisionVector)
					end
					local radiusVector = collisionVector:normalized(r)
					if self.solid then
						self.newPosition = self.newPosition + (collisionVector - radiusVector)
					end
				end
			end
		--end
	end
	self.position.x, self.position.y = self.newPosition.x, self.newPosition.y
end

function Actor:updateComponents(dt)
	for i, v in pairs(self.component) do
		if v.update then
			v:update(dt)
		end
	end
end

function Actor:draw()
	--love.graphics.circle(drawMode, math.floor(self.position.x/2)*2, math.floor(self.position.y/2)*2, self.radius)
	love.graphics.circle('fill', self.position.x, self.position.y, self.radius)
	--self:drawComponents()
end

function Actor:drawComponents()
    for i, v in pairs(self.component) do
    	if v.draw then
    		v:draw()
    	end
    end
end

return Actor
